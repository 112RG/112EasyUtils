package _112.easyutils.commands;

import _112.easyutils.Utils.Messages;
import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandSpeed implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(!(commandSender instanceof Player)){
            commandSender.sendMessage(Messages.youmustbeaplayer);
            return true;
        }

        Player player = (Player) commandSender;
        float speed = 1.0F;

        if(strings.length != 0 && NumberUtils.isDigits(strings[0])){
            if(Integer.valueOf(strings[0]) > 5){
                commandSender.sendMessage(Messages.commandprefix + ChatColor.RED +"Max speed is 5. Speed set to 5");
                speed = 5 / 5.0F;
                player.setFlySpeed(speed);
                return true;
            }
            speed = Integer.valueOf(strings[0]) / 5.0F;

            if(player.isFlying()){
                player.setFlySpeed(speed);
                commandSender.sendMessage(Messages.commandprefix + ChatColor.RED + "Set flying speed to " + ((int)(speed * 5)));
                return true;
            }
            else{
                player.setWalkSpeed(speed);
                commandSender.sendMessage(Messages.commandprefix + ChatColor.RED + "Set walking speed to " + ((int)(speed * 5)));
            }
            return true;
        }
            commandSender.sendMessage(Messages.commandprefix + ChatColor.RED +"You need to specify a speed");







        return true;
    }
}
