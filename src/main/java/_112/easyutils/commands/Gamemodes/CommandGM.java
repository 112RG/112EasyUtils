package _112.easyutils.commands.Gamemodes;

import _112.easyutils.Utils.CommandPrefix;
import _112.easyutils.Utils.Messages;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import _112.easyutils.Utils.CommandPrefix;

import java.util.Objects;

public class CommandGM implements CommandExecutor{
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!(commandSender instanceof Player)){
            commandSender.sendMessage(Messages.commandprefix + ChatColor.RED + "Must be a player!");
        }
        Player player = (Player) commandSender;

        if(Objects.equals(strings[0], "0") || Objects.equals(strings[0], "survival")){
            player.setGameMode(GameMode.SURVIVAL);
            commandSender.sendMessage(Messages.gamemodechange + player.getGameMode().toString());
        }
        else if(Objects.equals(strings[0], "1") || Objects.equals(strings[0], "creative")){
            player.setGameMode(GameMode.CREATIVE);
            commandSender.sendMessage(Messages.gamemodechange  + player.getGameMode().toString().toLowerCase());

        }
        else if(Objects.equals(strings[0], "2") || Objects.equals(strings[0], "adventure")){
            player.setGameMode(GameMode.ADVENTURE);
            commandSender.sendMessage(Messages.gamemodechange + player.getGameMode().toString().toLowerCase());

        }
        else if(Objects.equals(strings[0], "3") || Objects.equals(strings[0], "spectator")){
            player.setGameMode(GameMode.SPECTATOR);
            commandSender.sendMessage(Messages.gamemodechange + player.getGameMode().toString().toLowerCase());

        }
        else{
            player.sendMessage(Messages.specifygamemode + "Please specify a gamemode!");
        }

        return true;
    }
}
