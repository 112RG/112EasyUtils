package _112.easyutils;

import _112.easyutils.Utils.Messages;
import _112.easyutils.commands.CommandFly;
import _112.easyutils.commands.CommandGC;
import _112.easyutils.commands.CommandSpeed;
import _112.easyutils.commands.Command_112U;
import _112.easyutils.commands.Gamemodes.*;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import sun.applet.Main;

import java.io.File;

public final class _112Utils extends JavaPlugin {

    public static _112Utils plugin;



    @Override
    public void onEnable() {
        plugin = this;
        createConfig();
        getCommand("gc").setExecutor(new CommandGC());
        getCommand("gm").setExecutor(new CommandGM());
        getCommand("gmc").setExecutor(new CommandGMC());
        getCommand("gms").setExecutor(new CommandGMS());
        getCommand("gma").setExecutor(new CommandGMA());
        getCommand("gmsp").setExecutor(new CommandGMSP());
        getCommand("1u").setExecutor(new Command_112U());
        getCommand("fly").setExecutor(new CommandFly());
        getCommand("speed").setExecutor(new CommandSpeed());

    }

    @Override
    public void onDisable() {

    }

    private void createConfig(){
        try{
            if(!getDataFolder().exists()){
                getDataFolder().mkdirs();
            }
            File file = new File(getDataFolder(), "config.yml");

            if(!file.exists()){
                getLogger().info("Config.yml not found, creating!");
                saveDefaultConfig();
            } else{
                getLogger().info("Config.yml found, Loading!");
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }








}
